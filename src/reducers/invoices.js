import {
    ADD_INVOICE,
    UPDATE_INVOICE,
    REMOVE_INVOICE,
    SET_INVOICES,
} from 'actions/invoices';
import type { InvoicesState } from 'flow/types';

const initialState = [];

const invoices = (state: InvoicesState = initialState, action) => {
    switch (action.type) {
        case ADD_INVOICE: return [...state, action.payload.invoice];
        case UPDATE_INVOICE: {
            const invoiceIndex = state.findIndex(invoice => invoice.id === action.payload.id);
            if (invoiceIndex === -1) return state;
            return [
                ...state.slice(0, invoiceIndex),
                {
                    ...state[invoiceIndex],
                    ...action.payload.data,
                },
                ...state.slice(invoiceIndex + 1),
            ];
        }
        case REMOVE_INVOICE: {
            const invoiceIndex = state.findIndex(invoice => invoice.id === action.payload.id);
            if (invoiceIndex === -1) return state;
            return [
                ...state.slice(0, invoiceIndex),
                ...state.slice(invoiceIndex + 1),
            ];
        }
        case SET_INVOICES: return action.payload.invoices;
        default:
            return state;
    }
};

export default invoices;