// @flow

import uuid from 'uuid';
import type {
    Product,
    ProductConfig,
} from 'flow/types';

const createProduct = (config: ProductConfig = {}): Product => ({
    id: uuid(),
    name: '',
    purchasePrice: '',
    salePrice: '',
    ...config,
});

export default createProduct;
