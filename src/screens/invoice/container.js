// @flow

import { connect } from 'react-redux';
import View from './view';
import {
    addInvoice,
    removeInvoice,
    updateInvoice,
} from 'actions/invoices';
import generateObjectFromArray from 'utils/generate-object-from-array';

const mapStateToProps = (state: State) => ({
    clients: state.clients,
    clientsById: generateObjectFromArray(state.clients),
    invoices: state.invoices,
    invoiceTemplates: state.invoiceTemplates,
    invoiceTemplatesById: generateObjectFromArray(state.invoiceTemplates),
    products: state.products,
    productsById: generateObjectFromArray(state.products),
    sellers: state.sellers,
    sellersById: generateObjectFromArray(state.sellers),
});

const mapDispatchToProps = {
    addInvoice,
    removeInvoice,
    updateInvoice,
};

export default connect(mapStateToProps, mapDispatchToProps)(View);