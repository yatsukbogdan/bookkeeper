// @flow

import { connect } from 'react-redux';
import View from './view';
import type { State } from 'flow/types';
import generateObjectFromArray from 'utils/generate-object-from-array';

const mapStateToProps = (state: State) => ({
    clientsById: generateObjectFromArray(state.clients),
    invoiceTemplates: state.invoiceTemplates,
    invoiceTemplatesById: generateObjectFromArray(state.invoiceTemplates),
    sellersById: generateObjectFromArray(state.sellers),
});
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(View);