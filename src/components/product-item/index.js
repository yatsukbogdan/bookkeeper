// @flow

import React from 'react';
import {
    Text,
    View,
} from 'react-native';
import { Input } from 'react-native-elements';
import type { StyleProp } from 'react-native'
import styles from './styles'
import RoundButton from "./round-button";

type Props = {
    containerStyle: StyleProp,
    onAddPress: (quantity: number) => void,
    onPriceChange?: Function,
    onQuantityChange: Function,
    onRemovePress: Function,
    price?: string,
    quantity: string,
    showPrice: boolean,
    title: string,
}

const ProductItem = (props: Props) => (
    <View style={props.containerStyle}>
        <View style={styles.container}>
            <RoundButton
                containerStyle={{
                    zIndex: 10,
                    position: 'absolute',
                    top: 10,
                    right: 10,
                }}
                type='subtract'
                title='x'
                onPress={props.onRemovePress}
            />
            <Text style={[
                styles.title, {
                    fontWeight: 'bold',
                    marginBottom: props.showPrice ? 0 : 40,
                }
            ]}>
                {props.title}
            </Text>
            { props.showPrice && (
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginVertical: 10,
                }}>
                    <Text style={styles.title}>Price</Text>
                    <Input
                        keyboardType='numeric'
                        containerStyle={{ width: 150 }}
                        onChangeText={props.onPriceChange}
                        value={props.price}
                    />
                </View>
            )}
            <View style={styles.dataContainer}>
                <RoundButton
                    containerStyle={{ marginRight: 10 }}
                    type='subtract'
                    title='-5'
                    onPress={() => props.onAddPress(-5)}
                />
                <RoundButton
                    containerStyle={{ marginRight: 10 }}
                    type='subtract'
                    title='-1'
                    onPress={() => props.onAddPress(-1)}
                />
                <Input
                    keyboardType='numeric'
                    containerStyle={{
                        flex: 1,
                        marginRight: 10
                    }}
                    onChangeText={props.onQuantityChange}
                    value={props.quantity}
                />
                <RoundButton
                    containerStyle={{ marginRight: 10 }}
                    type='add'
                    title='+1'
                    onPress={() => props.onAddPress(1)}
                />
                <RoundButton
                    type='add'
                    title='+5'
                    onPress={() => props.onAddPress(5)}
                />
            </View>
        </View>
    </View>
);

ProductItem.defaultProps = {
    containerStyle: {},
};

export default ProductItem;