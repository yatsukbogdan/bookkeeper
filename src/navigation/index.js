import React from 'react';
import {
    createAppContainer,
    createBottomTabNavigator,
    createSwitchNavigator,
} from 'react-navigation';
import { Provider } from 'react-redux';
import ClientStackNavigator from './client-stack-navigator';
import InvoiceStackNavigator from './invoice-stack-navigator';
import ProductStackNavigator from './product-stack-navigator';
import SellerStackNavigator from './seller-stack-navigator';
import StockStackNavigator from './stock-stack-navigator';
import store from '../store';
import IoIcons from '@expo/vector-icons/Ionicons';
import API from 'api';
import { setSellers } from 'actions/sellers';
import { setProducts } from 'actions/products';
import { setClients } from 'actions/clients';
import { setInvoices } from 'actions/invoices';
import { setInvoiceTemplates } from 'actions/invoice-templates';

const AppContainer = createAppContainer(
    createSwitchNavigator({
        // You could add another route here for authentication.
        // Read more at https://reactnavigation.org/docs/en/auth-flow.html
        Main: createBottomTabNavigator({
            InvoiceStackNavigator,
            ProductStackNavigator,
            ClientStackNavigator,
            SellerStackNavigator,
            StockStackNavigator,
        }, {
            defaultNavigationOptions: ({ navigation }) => ({
                tabBarIcon: ({ focused, horizontal, tintColor }) => {
                    const { routeName } = navigation.state;
                    let iconName;
                    if (routeName === 'InvoiceStackNavigator') {
                        iconName = 'ios-journal';
                    } else if (routeName === 'ProductStackNavigator') {
                        iconName = 'ios-pizza';
                    } else if (routeName === 'ClientStackNavigator') {
                        iconName = 'ios-person';
                    } else if (routeName === 'SellerStackNavigator') {
                        iconName = 'ios-people';
                    } else if (routeName === 'StockStackNavigator') {
                        iconName = 'ios-document';
                    }

                    return <IoIcons name={iconName} size={25} color={tintColor} />;
                },
            }),
        }),
    }),
);

class App extends React.Component {
    async componentDidMount(): void {
        const stateFromApi = await API.get();
        store.dispatch(setClients(stateFromApi.clients));
        store.dispatch(setInvoices(stateFromApi.invoices));
        store.dispatch(setInvoiceTemplates(stateFromApi.invoiceTemplates));
        store.dispatch(setProducts(stateFromApi.products));
        store.dispatch(setSellers(stateFromApi.sellers));
        store.subscribe(() => {
            const state = store.getState();
            API.update(state);
        });
    }

    render() {
        return (
            <Provider store={store}>
                <AppContainer />
            </Provider>
        );
    }
}

export default App;