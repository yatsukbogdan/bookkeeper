// @flow

import { connect } from 'react-redux';
import View from './view';
import type { State } from 'flow/types';

const mapStateToProps = (state: State) => ({
    products: state.products
});
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(View);