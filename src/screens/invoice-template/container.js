// @flow

import { connect } from 'react-redux';
import View from './view';
import {
    addInvoiceTemplate,
    removeInvoiceTemplate,
    updateInvoiceTemplate,
} from 'actions/invoice-templates';
import generateObjectFromArray from 'utils/generate-object-from-array';

const mapStateToProps = (state: State) => ({
    clients: state.clients,
    clientsById: generateObjectFromArray(state.clients),
    invoiceTemplates: state.invoiceTemplates,
    products: state.products,
    productsById: generateObjectFromArray(state.products),
    sellers: state.sellers,
    sellersById: generateObjectFromArray(state.sellers),
});

const mapDispatchToProps = {
    addInvoiceTemplate,
    removeInvoiceTemplate,
    updateInvoiceTemplate,
};

export default connect(mapStateToProps, mapDispatchToProps)(View);