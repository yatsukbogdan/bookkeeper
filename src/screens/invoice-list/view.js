// @flow

import React from 'react';
import {
    DatePickerAndroid,
    DatePickerIOS,
    Platform,
    ScrollView,
    View,
} from 'react-native';
import { ListItem, Button } from 'react-native-elements';
import styles from './styles'
import type {
    Client,
    Invoice, InvoiceData,
    Seller,
} from 'flow/types';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import {INVOICE_TYPE_PURCHASE, INVOICE_TYPE_SALE} from 'constants/invoice';
import generatePdfMakeConfig from 'utils/generate-pdf-make-config';
import sharePdf from 'utils/share-pdf';

type Props = {
    clientsById: { [key: string]: Client },
    invoices: Array<Invoice>,
    invoicesById: { [key: string]: Invoice },
    sellersById: { [key: string]: Seller },
}

type State = {
    date: string,
}

class InvoiceListScreen extends React.Component<Props, State> {
    constructor (props) {
        super(props);

        const date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);

        this.state = {
            date: date.toISOString(),
        }
    }

    onDateChange = date => this.setState({ date });

    onDatePress = async () => {
        // Android Date Picker
        try {
            const { action, year, month, day } = await DatePickerAndroid.open({
                date: new Date(),
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                // Selected year, month (0-11), day
                this.onDateChange(new Date(year, month, day));
            }
        } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
        }
    };

    get filteredInvoices () {
        const currentDate = new Date(this.state.date);
        const nextDate = new Date(this.state.date);
        nextDate.setDate(nextDate.getDate() + 1);
        return this.props.invoices.filter(invoice => {
            const invoiceCreateDate = new Date(invoice.createDate);
            return invoiceCreateDate > currentDate && invoiceCreateDate < nextDate;
        });
    }

    getPdfInvoicesForToday = () => {
        if (this.filteredInvoices.length === 0) return;
        const date = new Date(this.filteredInvoices[0].createDate);
        date.setDate(date.getDate() + 1);
        const invoicesData = this.filteredInvoices.map(invoice => {
            const { type } = invoice;
            const seller = this.props.sellersById[invoice.sellerId];
            const client = this.props.clientsById[invoice.clientId];
            return {
                client: {
                    name: type === INVOICE_TYPE_SALE ? client.name : seller.name,
                },
                seller: {
                    name: type === INVOICE_TYPE_SALE ? seller.name : 'Хлібозавод',
                    phoneNumber: type === INVOICE_TYPE_SALE ? seller.phoneNumber : '',
                },
                date: date.toLocaleDateString('uk-Uk'),
                type: type === INVOICE_TYPE_SALE ? 'Расходная' : 'Приходная',
                productRecords: invoice.productRecords.map(record => ({
                    name: this.props.productsById[record.productId].name,
                    quantity: record.quantity,
                    price: record.price,
                }))
            };
        });
        pdfMake.createPdf(generatePdfMakeConfig(invoicesData)).getDataUrl(sharePdf, date.toLocaleDateString('uk-Uk'));
    };

    getInvoiceTitle = id => {
        const {
            clientsById,
            invoicesById,
            sellersById,
        } = this.props;
        const invoice = invoicesById[id];
        const customerName = invoice.type === INVOICE_TYPE_PURCHASE ?
            sellersById[invoice.sellerId].name :
            clientsById[invoice.clientId].name;
        const typeName = invoice.type === INVOICE_TYPE_PURCHASE ? 'Приходная' : 'Расходная';
        return `${customerName} - ${typeName}`;
    };

    render() {
        return (
            <View style={{flex: 1}}>
                <View style={styles.dateButtonsContainer}>
                    <Button
                        containerStyle={{ flex: 1 }}
                        onPress={this.onDatePress}
                        title={new Date(this.state.date).toLocaleDateString('uk-UK')}
                    />
                </View>
                <ScrollView style={styles.container}>
                    {this.filteredInvoices.map(invoice => (
                        <ListItem
                            key={invoice.id}
                            onPress={() => this.props.navigation.push('Invoice', { id: invoice.id })}
                            title={this.getInvoiceTitle(invoice.id)}
                        />
                    ))}
                </ScrollView>
                <Button
                    containerStyle={{
                        marginBottom: 10,
                        marginHorizontal: 10,
                    }}
                    onPress={() => this.props.navigation.push('Invoice')}
                    title='Создать накладную'
                />
                <Button
                    containerStyle={{
                        marginBottom: 10,
                        marginHorizontal: 10,
                    }}
                    onPress={() => this.props.navigation.push('InvoiceTemplateList')}
                    title='Открыть шаблоны'
                />
                <Button
                    containerStyle={{
                        marginBottom: 10,
                        marginHorizontal: 10,
                    }}
                    onPress={this.getPdfInvoicesForToday}
                    title='Накладные за сегодня (PDF)'
                />
                { Platform.OS === 'ios' && (
                    <DatePickerIOS
                        mode='date'
                        date={new Date(this.state.date)}
                        onDateChange={this.onDateChange}
                    />
                )}
            </View>
        )
    }
}

InvoiceListScreen.navigationOptions = {
    title: 'Список накладных',
};

export default InvoiceListScreen;