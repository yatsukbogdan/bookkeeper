// @flow

import { connect } from 'react-redux';
import View from './view';
import {
    addClient,
    removeClient,
    updateClient,
} from 'actions/clients';

const mapStateToProps = (state: State) => ({
    clients: state.clients,
});

const mapDispatchToProps = {
    addClient,
    removeClient,
    updateClient,
};

export default connect(mapStateToProps, mapDispatchToProps)(View);