import {
    ADD_INVOICE_TEMPLATE,
    UPDATE_INVOICE_TEMPLATE,
    REMOVE_INVOICE_TEMPLATE,
    SET_INVOICE_TEMPLATES,
} from 'actions/invoice-templates';
import type { InvoiceTemplatesState } from 'flow/types';

const initialState = [];

const invoiceTemplates = (state: InvoiceTemplatesState = initialState, action) => {
    switch (action.type) {
        case ADD_INVOICE_TEMPLATE: return [...state, action.payload.invoiceTemplate];
        case UPDATE_INVOICE_TEMPLATE: {
            const invoiceTemplateIndex = state.findIndex(invoiceTemplate => invoiceTemplate.id === action.payload.id);
            if (invoiceTemplateIndex === -1) return state;
            return [
                ...state.slice(0, invoiceTemplateIndex),
                {
                    ...state[invoiceTemplateIndex],
                    ...action.payload.data,
                },
                ...state.slice(invoiceTemplateIndex + 1),
            ];
        }
        case REMOVE_INVOICE_TEMPLATE: {
            const invoiceTemplateIndex = state.findIndex(invoiceTemplate => invoiceTemplate.id === action.payload.id);
            if (invoiceTemplateIndex === -1) return state;
            return [
                ...state.slice(0, invoiceTemplateIndex),
                ...state.slice(invoiceTemplateIndex + 1),
            ];
        }
        case SET_INVOICE_TEMPLATES: return action.payload.invoiceTemplates;
        default:
            return state;
    }
};

export default invoiceTemplates;