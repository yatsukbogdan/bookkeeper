// @flow

import React from 'react';
import { ScrollView, View } from 'react-native';
import { ListItem, Button } from 'react-native-elements';
import styles from './styles'
import type { Seller } from 'flow/types';

type Props = {
    sellers: Array<Seller>
}

const SellerListScreen = (props: Props) => (
    <View style={{flex: 1}}>
        <ScrollView style={styles.container}>
            {props.sellers.map(seller => (
                <ListItem
                    key={seller.id}
                    title={seller.name}
                    onPress={() => props.navigation.push('Seller', { id: seller.id })}
                />
            ))}
        </ScrollView>
        <Button
            containerStyle={{
                marginBottom: 10,
                marginHorizontal: 10,
            }}
            title='Добавить нового продавца'
            onPress={() => props.navigation.push('Seller')}
        />
    </View>
);

SellerListScreen.navigationOptions = {
    title: 'Список продавцов',
};

export default SellerListScreen;