import { createStackNavigator } from 'react-navigation';

import SellerListScreen from '../screens/seller-list';
import SellerScreen from '../screens/seller';

const SellerStackNavigator = createStackNavigator({
    SellerList: SellerListScreen,
    Seller: SellerScreen,
});

SellerStackNavigator.navigationOptions = {
    tabBarLabel: 'Продавцы'
};

SellerStackNavigator.path = '';

export default SellerStackNavigator;
