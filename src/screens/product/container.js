// @flow

import { connect } from 'react-redux';
import View from './view';
import {
    addProduct,
    removeProduct,
    updateProduct,
} from 'actions/products';

const mapStateToProps = (state: State) => ({
    products: state.products,
});

const mapDispatchToProps = {
    addProduct,
    removeProduct,
    updateProduct,
};

export default connect(mapStateToProps, mapDispatchToProps)(View);