const generateObjectFromArray = array => array.reduce((items, item) => {
    items[item.id] = item;
    return items;
}, {});

export default generateObjectFromArray;