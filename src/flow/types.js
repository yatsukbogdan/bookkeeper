// @flow
import {
    INVOICE_TYPE_PURCHASE,
    INVOICE_TYPE_SALE,
} from 'constants/invoice';

export type ProductConfig = {
    name: string,
    purchasePrice: string,
    salePrice: string,
}

export type Product = ProductConfig & {
    id: string,
}

export type ProductsState = Array<Product>;

export type ClientConfig = {
    name: string,
}

export type Client = ClientConfig & {
    id: string,
}

export type ClientsState = Array<Client>;

export type SellerConfig = {
    name: string,
    phoneNumber: string,
}

export type Seller = SellerConfig & {
    id: string,
}

export type SellersState = Array<Client>;

export type TemplateProductRecord = {
    productId: string,
    quantity: string,
}

export type ProductRecord = TemplateProductRecord & {
    price: string,
}

export type InvoiceType = INVOICE_TYPE_PURCHASE | INVOICE_TYPE_SALE;

export type InvoiceConfig = {
    clientId: ?string,
    createDate: string,
    productRecords: Array<ProductRecord>,
    sellerId: ?string,
    type: InvoiceType,
}

export type Invoice = InvoiceConfig & {
    id: string,
}

export type InvoicesState = Array<Invoice>;

export type InvoiceTemplateConfig = {
    clientId: ?string,
    name: ?string,
    productRecords: Array<TemplateProductRecord>,
    sellerId: ?string,
    type: InvoiceType,
    weekday: number,
}

export type InvoiceTemplate = InvoiceTemplateConfig & {
    id: string,
}

export type InvoiceTemplatesState = Array<InvoiceTemplate>;

export type InvoiceData = {
    client: {
        name: string,
    },
    seller: {
        name: string,
        phoneNumber: string,
    },
    date: string,
    type: string,
    productRecords: Array<{
        name: string,
        quantity: string,
        price: string,
    }>,
}

export type State = {
    clients: ClientsState,
    invoices: InvoicesState,
    invoiceTemplates: InvoiceTemplatesState,
    products: ProductsState,
    sellers: SellersState,
}