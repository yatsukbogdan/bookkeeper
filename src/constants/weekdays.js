export const WEEKDAY_MONDAY = 1;
export const WEEKDAY_TUESDAY = 2;
export const WEEKDAY_WEDNESDAY = 3;
export const WEEKDAY_THURSDAY = 4;
export const WEEKDAY_FRIDAY = 5;
export const WEEKDAY_SATURDAY = 6;
export const WEEKDAY_SUNDAY = 0;

export const WEEKDAY_OPTIONS = [{
    id: WEEKDAY_MONDAY,
    name: 'Monday',
}, {
    id: WEEKDAY_TUESDAY,
    name: 'Tuesday',
}, {
    id: WEEKDAY_WEDNESDAY,
    name: 'Wednesday',
}, {
    id: WEEKDAY_THURSDAY,
    name: 'Thursday',
}, {
    id: WEEKDAY_FRIDAY,
    name: 'Friday',
}, {
    id: WEEKDAY_SATURDAY,
    name: 'Saturday',
}, {
    id: WEEKDAY_SUNDAY,
    name: 'Sunday',
}];
