import {
    Seller,
    SellerConfig,
} from 'flow/types';

export const ADD_SELLER = 'ADD_SELLER';
export const UPDATE_SELLER = 'UPDATE_SELLER';
export const REMOVE_SELLER = 'REMOVE_SELLER';
export const SET_SELLERS = 'SET_SELLERS';

export const addSeller = (seller: Seller) => ({
    type: ADD_SELLER,
    payload: {
        seller
    }
});

export const updateSeller = (id: string, data: SellerConfig) => ({
    type: UPDATE_SELLER,
    payload: {
        id,
        data,
    }
});

export const removeSeller = (id: string) => ({
    type: REMOVE_SELLER,
    payload: {
        id
    }
});

export const setSellers = (sellers: Array<Seller>) => ({
    type: SET_SELLERS,
    payload: {
        sellers
    }
});