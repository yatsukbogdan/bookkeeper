import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        padding: 10,
        borderRadius: 10,
        backgroundColor: '#eee',
    },
    dataContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    title: {
        fontSize: 20,
    },
});