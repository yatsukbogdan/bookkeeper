// @flow

import React from 'react';
import {
    Alert,
    DatePickerAndroid,
    DatePickerIOS,
    KeyboardAvoidingView,
    Modal,
    Platform,
    ScrollView,
    Share,
    View,
} from 'react-native';
import {
    Button,
    ButtonGroup,
} from 'react-native-elements';
import styles from './styles'
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import createInvoice from 'utils/invoices/create-invoice';
import type {
    Client,
    Invoice,
    InvoiceConfig, InvoiceData,
    InvoiceTemplate,
    InvoiceType,
    Product,
    ProductRecord,
    Seller,
} from 'flow/types';
import {
    INVOICE_TYPE_PURCHASE,
    INVOICE_TYPE_SALE,
} from 'constants/invoice';
import EntityButton from 'components/entity-button';
import ProductItem from 'components/product-item';
import ItemPicker from 'components/item-picker';
import generatePdfMakeConfig from 'utils/generate-pdf-make-config';
import sharePdf from 'utils/share-pdf';

type Props = {
    addInvoice: (product: Invoice) => void,
    clients: Array<Client>,
    clientsById: { [key: string]: Client },
    invoices: Array<Invoice>,
    invoiceTemplates: Array<InvoiceTemplate>,
    invoiceTemplatesById: { [key: string]: InvoiceTemplate },
    products: Array<Product>,
    productsById: { [key: string]: Product },
    removeInvoice: (id: string) => void,
    sellers: Array<Seller>,
    sellersById: { [key: string]: Seller },
    updateInvoice: (id: string, data: InvoiceConfig) => void,
}

type State = {
    clientId: ?string,
    clientsModalVisible: boolean,
    createDate: string,
    id: string,
    productRecords: Array<ProductRecord>,
    productsModalVisible: boolean,
    sellerId: ?string,
    sellersModalVisible: boolean,
    templateModalVisible: boolean,
    type: InvoiceType,
}

class InvoiceScreen extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        const id = this.props.navigation.getParam('id');
        const invoice = this.props.invoices.find(invoice => invoice.id === id);
        this.state = {
            clientId: invoice ? invoice.clientId : null,
            clientsModalVisible: false,
            createDate: new Date().toISOString(),
            id,
            productRecords: invoice ? invoice.productRecords : [],
            productsModalVisible: false,
            sellerId: invoice ? invoice.sellerId : null,
            sellersModalVisible: false,
            templateModalVisible: false,
            type: invoice ? invoice.type : INVOICE_TYPE_SALE,
        };
    }

    onClientPickerPress = clientId => {
        this.setState({
            clientsModalVisible: false,
            clientId,
        }, () => setTimeout(this.onInvoiceChange, 100));
    };

    onDateChange = createDate => this.setState({ createDate }, this.onInvoiceChange);

    onDatePress = async () => {
        // Android Date Picker
        try {
            const { action, year, month, day } = await DatePickerAndroid.open({
                date: new Date(),
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                // Selected year, month (0-11), day
                this.onDateChange(new Date(year, month, day));
            }
        } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
        }
    };

    showClientsModal = () => this.setState({ clientsModalVisible: true });

    hideClientsModal = () => this.setState({ clientsModalVisible: false });

    onSellerPickerPress = sellerId => {
        this.setState({
            sellersModalVisible: false,
            sellerId,
        }, () => setTimeout(this.onInvoiceChange, 100));
    };

    showSellersModal = () => this.setState({ sellersModalVisible: true });

    hideSellersModal = () => this.setState({ sellersModalVisible: false });

    applyTemplate = templateId => {
        const template = this.props.invoiceTemplatesById[templateId];
        this.setState({
            productRecords: template.productRecords.map(record => {
                const product = this.props.productsById[record.productId];
                const price = this.state.type === INVOICE_TYPE_SALE ? product.salePrice : product.purchasePrice;
                return {
                    productId: record.productId,
                    quantity: record.quantity,
                    price,
                }
            })
        });
    };

    onTemplatePickerPress = templateId => {
        this.hideTemplateModal();
        this.applyTemplate(templateId);
    };

    showTemplateModal = () => this.setState({ templateModalVisible: true });

    hideTemplateModal = () => this.setState({ templateModalVisible: false });

    onProductPickerPress = productId => {
        const product = this.props.productsById[productId];
        const price = this.state.type === INVOICE_TYPE_PURCHASE ? product.purchasePrice : product.salePrice;
        const productRecords = [...this.state.productRecords, {
            price,
            productId,
            quantity: '0',
        }];
        this.setState({ productRecords });
        this.hideProductsModal();
    };

    showProductsModal = () => this.setState({ productsModalVisible: true });

    hideProductsModal = () => this.setState({ productsModalVisible: false });

    onProductQuantityAddPress = (quantityToAdd, index) => {
        const quantity = Number(this.state.productRecords[index].quantity) + quantityToAdd;
        const newQuantity = quantity < 0 ? 0 : quantity;
        this.onProductRecordFieldChange('quantity', String(newQuantity), index);
    };

    onProductRecordFieldChange = (field, value, index) => {
        this.setState({ productRecords: [
                ...this.state.productRecords.slice(0, index),
                {
                    ...this.state.productRecords[index],
                    [field]: value,
                },
                ...this.state.productRecords.slice(index + 1),
            ],
        });
    };

    onProductRecordRemovePress = index => {
        this.setState({
            productRecords: [
                ...this.state.productRecords.slice(0, index),
                ...this.state.productRecords.slice(index + 1),
            ]
        });
    };

    onInvoiceTypePress = type => {
        if (type === INVOICE_TYPE_PURCHASE) {
            this.setState({ clientId: null });
        }
        this.setState({ type }, this.onInvoiceChange);
    };

    onTemplateAlertYesPress = () => {
        if (this.suitableTemplates.length === 1) {
            this.applyTemplate(this.suitableTemplates[0].id);
            return;
        }
        this.showTemplateModal();
    };

    onInvoiceChange = () => {
        const { suitableTemplates } = this;
        if (suitableTemplates.length !== 0) {
            Alert.alert(
                'Хотите применить шаблон к этой накладной?',
                `Доступные шаблоны: \n\n${suitableTemplates.map(template => template.name).join('\n')}`,
                [{
                    style: 'cancel',
                    text: 'No',
                }, {
                    style: 'default',
                    text: 'Yes',
                    onPress: this.onTemplateAlertYesPress
                }]
            );
        }
    };

    get suitableTemplates () {
        const {
            clientId,
            createDate,
            sellerId,
            type,
        } = this.state;
        const date = new Date(createDate);
        date.setDate(date.getDate() + 1);
        const weekday = date.getDay();
        return this.props.invoiceTemplates.filter(template => {
            return template.weekday === weekday &&
                   template.sellerId === sellerId &&
                   template.clientId === clientId &&
                   template.type === type;
        });
    }

    generatePdf = () => {
        const { type } = this.state;
        const seller = this.props.sellersById[this.state.sellerId];
        const client = this.props.clientsById[this.state.clientId];
        const date = new Date(this.state.createDate);
        date.setDate(date.getDate() + 1);
        const invoiceData: InvoiceData = {
            client: {
                name: type === INVOICE_TYPE_SALE ? client.name : seller.name,
            },
            seller: {
                name: type === INVOICE_TYPE_SALE ? seller.name : 'Хлібозавод',
                phoneNumber: type === INVOICE_TYPE_SALE ? seller.phoneNumber : '',
            },
            date: date.toLocaleDateString('uk-Uk'),
            type: type === INVOICE_TYPE_SALE ? 'Расходная' : 'Приходная',
            productRecords: this.state.productRecords.map(record => ({
                name: this.props.productsById[record.productId].name,
                quantity: record.quantity,
                price: record.price,
            }))
        };
        pdfMake.createPdf(generatePdfMakeConfig([ invoiceData ])).getDataUrl(sharePdf, date.toLocaleDateString('uk-Uk'),);
    };

    clearForm = () =>
        this.setState({
            clientId: null,
            clientsModalVisible: false,
            createDate: new Date().toISOString(),
            productRecords: [],
            productsModalVisible: false,
            sellerId: null,
            sellersModalVisible: false,
            templateModalVisible: false,
            type: INVOICE_TYPE_SALE,
        });

    get isFormValid () {
        const isClientFieldValid = this.state.type === INVOICE_TYPE_PURCHASE || this.state.clientId !== null;
        return this.state.productRecords.length > 0 && isClientFieldValid && this.state.sellerId !== null;
    }

    onSubmitButtonPress = () => {
        if (!this.isFormValid) return;

        if (this.state.id) {
            this.onSavePress();
            return;
        }
        this.onAddPress();
    };

    get availableProducts () {
        const chosenProductsIds = this.state.productRecords.map(productRecord => productRecord.productId);
        return this.props.products.filter(product => !chosenProductsIds.includes(product.id));
    }

    get invoiceConfig (): InvoiceConfig {
        return {
            clientId: this.state.clientId,
            createDate: this.state.createDate,
            productRecords: this.state.productRecords,
            sellerId: this.state.sellerId,
            type: this.state.type,
        }
    }

    onAddPress = () => {
        this.props.addInvoice(createInvoice(this.invoiceConfig));
        this.clearForm();
        this.props.navigation.goBack();
    };

    onSavePress = () => {
        this.props.updateInvoice(this.state.id, this.invoiceConfig);
        this.props.navigation.goBack();
    };

    render () {
        const {
            clients,
            clientsById,
            productsById,
            sellers,
            sellersById,
        } = this.props;
        const {
            clientId,
            productRecords,
            sellerId,
        } = this.state;
        const client = clientsById[clientId];
        const seller = sellersById[sellerId];

        return (
            <View style={styles.container}>
                <KeyboardAvoidingView style={{ flex: 1 }} behavior='height'>
                    <ScrollView style={{ flex: 1 }}>
                        <ButtonGroup
                            onPress={this.onInvoiceTypePress}
                            selectedIndex={this.state.type}
                            buttons={['Приходная', 'Расходная']}
                            containerStyle={{ marginBottom: 10 }}
                        />
                        { this.state.type === INVOICE_TYPE_SALE && (
                            <EntityButton
                                containerStyle={{ marginBottom: 20 }}
                                onPress={this.showClientsModal}
                                title='Клиент'
                                subtitle={client ? client.name : ''}
                            />
                        )}
                        <EntityButton
                            containerStyle={{ marginBottom: 20 }}
                            onPress={this.showSellersModal}
                            title='Продавец'
                            subtitle={seller ? seller.name : ''}
                        />
                        <EntityButton
                            containerStyle={{ marginBottom: 20 }}
                            onPress={this.onDatePress}
                            title='Дата'
                            subtitle={new Date(this.state.createDate).toLocaleDateString('uk-Uk')}
                        />
                        { Platform.OS === 'ios' && (
                            <DatePickerIOS
                                mode='date'
                                date={new Date(this.state.createDate)}
                                onDateChange={this.onDateChange}
                            />
                        )}
                        {productRecords.map((productRecord, index) => (
                            <ProductItem
                                containerStyle={{ marginBottom: 20 }}
                                key={productRecord.productId}
                                onAddPress={value => this.onProductQuantityAddPress(value, index)}
                                onPriceChange={value => this.onProductRecordFieldChange('price', value, index)}
                                onQuantityChange={value => this.onProductRecordFieldChange('quantity', value, index)}
                                onRemovePress={() => this.onProductRecordRemovePress(index)}
                                price={productRecord.price}
                                quantity={productRecord.quantity}
                                showPrice
                                title={productsById[productRecord.productId].name}
                            />
                        ))}
                    </ScrollView>
                    <Button
                        containerStyle={{ marginBottom: 10 }}
                        onPress={this.showProductsModal}
                        title='Добавить товар'
                    />
                    <Button
                        onPress={this.generatePdf}
                        title='Получить PDF'
                        containerStyle={{ marginBottom: 10 }}
                    />
                    <Button
                        onPress={this.onSubmitButtonPress}
                        buttonStyle={{ backgroundColor: '#9add83' }}
                        title='Сохранить'
                    />
                </KeyboardAvoidingView>
                <Modal visible={this.state.clientsModalVisible}>
                    <ItemPicker
                        items={clients}
                        onPress={this.onClientPickerPress}
                        close={this.hideClientsModal}
                    />
                </Modal>
                <Modal visible={this.state.sellersModalVisible}>
                    <ItemPicker
                        items={sellers}
                        onPress={this.onSellerPickerPress}
                        close={this.hideSellersModal}
                    />
                </Modal>
                <Modal visible={this.state.productsModalVisible}>
                    <ItemPicker
                        items={this.availableProducts}
                        onPress={this.onProductPickerPress}
                        close={this.hideProductsModal}
                    />
                </Modal>
                <Modal visible={this.state.templateModalVisible}>
                    <ItemPicker
                        items={this.suitableTemplates}
                        onPress={this.onTemplatePickerPress}
                        close={this.hideTemplateModal}
                    />
                </Modal>
            </View>
        );
    }
}

InvoiceScreen.navigationOptions = {
    title: 'Накладная',
};

export default InvoiceScreen;