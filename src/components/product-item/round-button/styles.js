import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#eee',
        borderRadius: 25,
        height: 50,
        justifyContent: 'center',
        width: 50,
    },
    title: {
        fontSize: 20,
    },
    subtitle: {
        fontSize: 20,
        color: '#555',
    }
});