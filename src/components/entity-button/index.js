// @flow

import React from 'react';
import {
    TouchableOpacity,
    View,
    Text,
} from 'react-native';
import type { StyleProp } from 'react-native'
import styles from './styles'

type Props = {
    onPress: Function,
    title: string,
    subtitle: string,
    containerStyle: StyleProp,
}

const EntityButton = (props: Props) => (
    <View style={props.containerStyle}>
        <TouchableOpacity onPress={props.onPress}>
            <View style={styles.container}>
                <Text style={styles.title}>{props.title}</Text>
                <Text style={styles.subtitle}>{props.subtitle}</Text>
            </View>
        </TouchableOpacity>
    </View>
);

EntityButton.defaultProps = {
    containerStyle: {},
};

export default EntityButton;