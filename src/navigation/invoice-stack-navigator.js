import { createStackNavigator } from 'react-navigation';

import InvoiceListScreen from 'screens/invoice-list';
import InvoiceScreen from 'screens/invoice';
import InvoiceTemplateListScreen from 'screens/invoice-template-list';
import InvoiceTemplateScreen from 'screens/invoice-template';

const InvoiceStackNavigator = createStackNavigator({
    Invoice: InvoiceScreen,
    InvoiceList: InvoiceListScreen,
    InvoiceTemplate: InvoiceTemplateScreen,
    InvoiceTemplateList: InvoiceTemplateListScreen,
}, {
    initialRouteName: 'InvoiceList'
});

InvoiceStackNavigator.navigationOptions = {
    tabBarLabel: 'Накладные'
};

InvoiceStackNavigator.path = '';

export default InvoiceStackNavigator;
