import {
    ADD_CLIENT,
    REMOVE_CLIENT,
    UPDATE_CLIENT,
    SET_CLIENTS,
} from 'actions/clients';
import type { ClientsState } from 'flow/types';

const initialState = [];

const clients = (state: ClientsState = initialState, action) => {
    switch (action.type) {
        case ADD_CLIENT: return [...state, action.payload.client];
        case UPDATE_CLIENT: {
            const clientIndex = state.findIndex(client => client.id === action.payload.id);
            if (clientIndex === -1) return state;
            return [
                ...state.slice(0, clientIndex),
                {
                    ...state[clientIndex],
                    ...action.payload.data,
                },
                ...state.slice(clientIndex + 1),
            ];
        }
        case REMOVE_CLIENT: {
            const clientIndex = state.findIndex(client => client.id === action.payload.id);
            if (clientIndex === -1) return state;
            return [
                ...state.slice(0, clientIndex),
                ...state.slice(clientIndex + 1),
            ];
        }
        case SET_CLIENTS: return action.payload.clients;
        default:
            return state;
    }
};

export default clients;