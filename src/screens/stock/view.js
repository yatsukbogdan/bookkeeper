// @flow

import React from 'react';
import { ScrollView, Text } from 'react-native';
import styles from './styles'
import {
    INVOICE_TYPE_PURCHASE,
} from 'constants/invoice'
import type {
    Invoice,
    Product,
} from 'flow/types';

type Props = {
    invoices: Array<Invoice>,
    productsById: { [key: string]: Product },
}

class StockScreen extends React.Component<Props> {
    get stock () {
        const stockWithEmptyRecords = this.props.invoices.reduce((stock, invoice) => {
            invoice.productRecords.forEach(record => {
                const quantityMultiplier = invoice.type === INVOICE_TYPE_PURCHASE ? 1 : -1;
                const quantityToAdd = quantityMultiplier * record.quantity;
                if (!stock[record.productId]) {
                    stock[record.productId] = quantityToAdd;
                    return;
                }
                stock[record.productId] += quantityToAdd;
            });
            return stock;
        }, {});
        return Object.keys(stockWithEmptyRecords)
            .filter(productId => stockWithEmptyRecords[productId] !== 0)
            .reduce((stock, productId) => {
                stock[productId] = stockWithEmptyRecords[productId];
                return stock;
            }, {});
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                {Object.keys(this.stock).map(productId => (
                    <Text
                        key={productId}
                        style={{ marginBottom: 20 }}
                    >
                        {this.props.productsById[productId].name}: {this.stock[productId]}
                    </Text>
                ))}
            </ScrollView>
        )
    }
}

StockScreen.navigationOptions = {
    title: 'Stock',
};

export default StockScreen;