import {
    Platform,
    Share,
} from "react-native";
import * as FileSystem from "expo-file-system";
import * as MailComposer from "expo-mail-composer";

const sharePdf = (url, date) => {
    if (Platform.OS === 'ios') {
        Share.share({ url });
        return;
    }
    FileSystem.writeAsStringAsync(
        `${FileSystem.documentDirectory}tmp_invoice_file.pdf`,
        url.substring(28),
        {
            encoding: FileSystem.EncodingType.Base64
        }
    ).then(() => {
        MailComposer.composeAsync({
            recipients: ['yatsuksvetlana@ukr.net'],
            subject: `Invoices for ${date}`,
            attachments: [`${FileSystem.documentDirectory}tmp_invoice_file.pdf`],
        })
    });
};

export default sharePdf;