// @flow

import React from 'react';
import { ScrollView } from 'react-native';
import {
    Input,
    Button,
} from 'react-native-elements';
import styles from './styles'
import createSeller from 'utils/sellers/create-seller';
import type {
    Seller,
    SellerConfig,
} from 'flow/types';

type Props = {
    addSeller: (product: Seller) => void,
    sellers: Array<Seller>,
    removeSeller: (id: string) => void,
    updateSeller: (id: string, data: SellerConfig) => void,
}

type State = {
    id: string,
    nameError: string,
    nameValue: string,
    phoneNumberValue: string,
    phoneNumberError: string,
}

class SellerScreen extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        const id = this.props.navigation.getParam('id');
        const seller = this.props.sellers.find(seller => seller.id === id);
        this.state = {
            id,
            nameError: '',
            nameValue: seller ? seller.name : '',
            phoneNumberError: '',
            phoneNumberValue: seller ? seller.phoneNumber : '',
        };
    }

    validateName = () => {
        const value = this.state.nameValue;
        if (value.length < 2) {
            this.setState({ nameError: 'Имя должно быть длинее чем 2 символа' });
            return;
        }
        const otherSellers = this.state.id ?
            this.props.sellers.filter(seller => seller.id !== this.state.id) :
            this.props.sellers;

        if (otherSellers.find(seller => seller.name === value)) {
            this.setState({ nameError: 'Продавец с таким именем уже существует' });
            return;
        }
        this.setState({ nameError: '' });
    };

    validatePhoneNumber = () => {

    };

    validateField = field => {
        switch (field) {
            case 'name': return this.validateName();
            case 'phoneNumber': return this.validatePhoneNumber();
            default: return;
        }
    };

    onChange = (field, value) => this.setState({
        [`${field}Value`]: value
    }, () => this.validateField(field));

    clearForm = () =>
        this.setState({
            nameError: '',
            nameValue: '',
            phoneNumberError: '',
            phoneNumberValue: '',
        });

    get isFormValid () {
        return !this.state.nameError && !this.state.phoneNumberError;
    }

    onSubmitButtonClick = () => {
        if (!this.isFormValid) return;

        if (this.state.id) {
            this.onSaveClick();
            return;
        }
        this.onAddClick();
    };

    get sellerConfig (): SellerConfig {
        return {
            name: this.state.nameValue,
            phoneNumber: this.state.phoneNumberValue,
        }
    }

    onAddClick = () => {
        const seller = createSeller(this.sellerConfig);
        this.props.addSeller(seller);
        this.clearForm();
        this.props.navigation.goBack();
    };

    onSaveClick = () => {
        this.props.updateSeller(this.state.id, this.sellerConfig);
        this.props.navigation.goBack();
    };

    render () {
        return (
            <ScrollView style={styles.container}>
                <Input
                    containerStyle={{ marginBottom: 20 }}
                    errorMessage={this.state.nameError}
                    label='Имя'
                    onChangeText={value => this.onChange('name', value)}
                    onSubmitEditing={() => this.phoneNumber.focus()}
                    returnKeyType='next'
                    value={this.state.nameValue}
                />
                <Input
                    containerStyle={{ marginBottom: 20 }}
                    errorMessage={this.state.phoneNumberError}
                    keyboardType='numeric'
                    label='Номер телефона'
                    onChangeText={value => this.onChange('phoneNumber', value)}
                    ref={input => this.phoneNumber = input}
                    returnKeyType='done'
                    value={this.state.phoneNumberValue}
                />
                <Button
                    containerStyle={{ marginHorizontal: 10 }}
                    onPress={this.onSubmitButtonClick}
                    title='Сохранить'
                    buttonStyle={{ backgroundColor: '#9add83' }}
                />
            </ScrollView>
        );
    }
}

SellerScreen.navigationOptions = {
    title: 'Seller',
};

export default SellerScreen;