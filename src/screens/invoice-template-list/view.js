// @flow

import React from 'react';
import {
    ScrollView,
    View,
} from 'react-native';
import { ListItem, Button } from 'react-native-elements';
import styles from './styles'
import type {
    Client,
    InvoiceTemplate,
    Seller,
} from 'flow/types';

type Props = {
    clientsById: { [key: string]: Client },
    invoiceTemplates: Array<InvoiceTemplate>,
    invoiceTemplatesById: { [key: string]: InvoiceTemplate },
    sellersById: { [key: string]: Seller },
}

class InvoiceTemplateListScreen extends React.Component<Props, State> {
    render() {
        return (
            <View style={{flex: 1}}>
                <ScrollView style={styles.container}>
                    {this.props.invoiceTemplates.map(invoiceTemplate => (
                        <ListItem
                            key={invoiceTemplate.id}
                            onPress={() => this.props.navigation.push('InvoiceTemplate', { id: invoiceTemplate.id })}
                            title={invoiceTemplate.name}
                        />
                    ))}
                </ScrollView>
                <Button
                    containerStyle={{
                        marginBottom: 10,
                        marginHorizontal: 10,
                    }}
                    onPress={() => this.props.navigation.push('InvoiceTemplate')}
                    title='Добавить новый шаблон'
                />
            </View>
        )
    }
}

InvoiceTemplateListScreen.navigationOptions = {
    title: 'Список шаблонов накалдных',
};

export default InvoiceTemplateListScreen;