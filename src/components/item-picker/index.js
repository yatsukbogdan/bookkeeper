// @flow

import React from 'react';
import {
    ScrollView,
    SafeAreaView,
} from 'react-native';
import {
    ListItem,
    SearchBar,
    Button,
} from 'react-native-elements';
import styles from './styles'
import type {
    Client,
    Product,
    Seller,
} from 'flow/types';
import Fuse from 'fuse.js';
import getDefaultFuseOptions from 'constants/fuse-options';

type Props = {
    close: Function,
    items: Array<Client> | Array<Product> | Array<Seller>,
    onPress: (id: string) => void,
}

type State = {
    searchInput: string,
}

class ItemPicker extends React.Component<Props, State> {
    state = {
        searchInput: '',
    };

    onSearchInputChange = searchInput => this.setState({ searchInput });

    get filteredItems () {
        if (this.state.searchInput.length === 0) return this.props.items;
        const fuse = new Fuse(this.props.items, getDefaultFuseOptions());
        return fuse.search(this.state.searchInput);
    }

    render () {
        return (
            <SafeAreaView style={{flex: 1}}>
                <Button
                    title='Back'
                    onPress={this.props.close}
                    containerStyle={{ margin: 20 }}
                />
                <SearchBar
                    lightTheme
                    onChangeText={this.onSearchInputChange}
                    placeholder='Type item name...'
                    value={this.state.searchInput}
                />
                <ScrollView style={styles.container}>
                    {this.filteredItems.map(item => (
                        <ListItem
                            key={item.id}
                            title={item.name}
                            onPress={() => this.props.onPress(item.id)}
                        />
                    ))}
                </ScrollView>
            </SafeAreaView>
        );
    }
}

export default ItemPicker;