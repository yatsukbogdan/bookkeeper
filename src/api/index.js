import axios from 'axios';
import {
    SECRET_KEY,
    JSON_BIN_ID,
} from '../../json-bin-config';

class API {
    static async get () {
        return (await axios.get(
        `https://api.jsonbin.io/b/${JSON_BIN_ID}/latest`,
        {
            headers: {
                'secret-key': SECRET_KEY,
            },
        })).data;
    }

    static update (data) {
        console.log(data);
        axios.put(
            `https://api.jsonbin.io/b/${JSON_BIN_ID}`,
            data,
            {
            headers: {
                'Content-Type': 'application/json',
                'secret-key': SECRET_KEY,
            },
        }).then(res => console.log(res));
    }
}

export default API;