// @flow

import React from 'react';
import { ScrollView, View } from 'react-native';
import { ListItem, Button } from 'react-native-elements';
import styles from './styles'
import type { Client } from 'flow/types';

type Props = {
    clients: Array<Client>
}

const ClientListScreen = (props: Props) => (
    <View style={{flex: 1}}>
        <ScrollView style={styles.container}>
            {props.clients.map(client => (
                <ListItem
                    key={client.id}
                    title={client.name}
                    onPress={() => props.navigation.push('Client', { id: client.id })}
                />
            ))}
        </ScrollView>
        <Button
            containerStyle={{
                marginBottom: 10,
                marginHorizontal: 10,
            }}
            onPress={() => props.navigation.push('Client')}
            title='Добавить нового клиента'
        />
    </View>
);

ClientListScreen.navigationOptions = {
    title: 'Список клиентов',
};

export default ClientListScreen;