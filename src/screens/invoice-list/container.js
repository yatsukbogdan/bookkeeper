// @flow

import { connect } from 'react-redux';
import View from './view';
import type { State } from 'flow/types';
import generateObjectFromArray from 'utils/generate-object-from-array';

const mapStateToProps = (state: State) => ({
    clientsById: generateObjectFromArray(state.clients),
    invoices: state.invoices,
    invoicesById: generateObjectFromArray(state.invoices),
    sellersById: generateObjectFromArray(state.sellers),
    productsById: generateObjectFromArray(state.products),
});
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(View);