import { createStackNavigator } from 'react-navigation';

import ProductListScreen from '../screens/product-list';
import ProductScreen from '../screens/product';

const ProductStackNavigator = createStackNavigator({
    ProductList: ProductListScreen,
    Product: ProductScreen,
});

ProductStackNavigator.navigationOptions = {
    tabBarLabel: 'Продукты'
};

ProductStackNavigator.path = '';

export default ProductStackNavigator;
