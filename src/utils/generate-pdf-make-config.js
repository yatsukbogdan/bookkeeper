// @flow
import type { InvoiceData } from 'flow/types';

const generatePdfMakeConfig = (invoicesData: Array<InvoiceData>) => {
    const contentArrays = invoicesData.map((invoiceData, index) => ([
        {
            table: {
                widths: [100, '*', '*'],
                headerRows: 1,
                body: [[{
                    text: 'Поставщик',
                    bold: true
                }, {
                    text: ` ${invoiceData.seller.name}\nтел. ${invoiceData.seller.phoneNumber}`
                }], [{
                    text: 'Получатель',
                    bold: true
                }, {
                    text: invoiceData.client.name
                }]]
            },
            layout: 'noBorders'
        }, {
            text: `${invoiceData.type} накладная от ${invoiceData.date}`,
            alignment: 'center',
            bold: true,
            margin: [20, 20]
        }, {
            table: {
                widths: [20, '*', 80, 50, 50],
                headerRows: 1,
                body: [[{
                    text: '№',
                    bold: true
                }, {
                    text: 'Товар',
                    bold: true
                }, {
                    text: 'Количество',
                    bold: true
                }, {
                    text: 'Цена',
                    bold: true
                }, {
                    text: 'Сумма',
                    bold: true
                }],
                ...invoiceData.productRecords.map((productRecord, index) => [
                    String(index + 1),
                    productRecord.name,
                    String(productRecord.quantity),
                    String(productRecord.price),
                    String(productRecord.price * productRecord.quantity),
                ]),
                [{
                    colSpan: 4,
                    text: 'Всего',
                    alignment: 'right'
                }, '', '', '', String(invoiceData.productRecords.reduce((sum, record) => sum + record.price * record.quantity, 0))]],
            },
            pageBreak: index === invoicesData.length - 1 ? null : 'after',
        },
    ]));
    const content = [];
    contentArrays.forEach(array => content.push(...array));
    return { content };
};

export default generatePdfMakeConfig;