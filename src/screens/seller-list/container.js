// @flow

import { connect } from 'react-redux';
import View from './view';
import type { State } from 'flow/types';

const mapStateToProps = (state: State) => ({
    sellers: state.sellers,
});
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(View);