import { combineReducers } from 'redux';
import clients from './clients';
import invoices from './invoices';
import invoiceTemplates from './invoice-templates';
import products from './products';
import sellers from './sellers';

export default combineReducers({
    clients,
    invoices,
    invoiceTemplates,
    products,
    sellers,
});
