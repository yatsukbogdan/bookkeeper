// @flow

import React from 'react';
import {
    KeyboardAvoidingView,
    Modal,
    ScrollView,
    View,
} from 'react-native';
import {
    Button,
    ButtonGroup,
    Input,
} from 'react-native-elements';
import styles from './styles'
import createInvoiceTemplate from 'utils/invoice-templates/create-invoice-template';
import type {
    Client,
    InvoiceTemplate,
    InvoiceTemplateConfig,
    InvoiceType,
    Product,
    TemplateProductRecord,
    Seller,
} from 'flow/types';
import {
    INVOICE_TYPE_PURCHASE,
    INVOICE_TYPE_SALE,
} from 'constants/invoice';
import EntityButton from 'components/entity-button';
import ProductItem from 'components/product-item';
import ItemPicker from 'components/item-picker';
import { WEEKDAY_OPTIONS } from 'constants/weekdays';
import getWeekdayName from 'utils/get-weekday-name';

type Props = {
    addInvoiceTemplate: (product: InvoiceTemplate) => void,
    clients: Array<Client>,
    clientsById: { [key: string]: Client },
    invoiceTemplates: Array<InvoiceTemplate>,
    products: Array<Product>,
    productsById: { [key: string]: Product },
    removeInvoiceTemplate: (id: string) => void,
    sellers: Array<Seller>,
    sellersById: { [key: string]: Seller },
    updateInvoiceTemplate: (id: string, data: InvoiceTemplateConfig) => void,
}

type State = {
    clientId: ?string,
    clientsModalVisible: boolean,
    id: string,
    name: string,
    productRecords: Array<TemplateProductRecord>,
    productsModalVisible: boolean,
    sellerId: ?string,
    sellersModalVisible: boolean,
    type: InvoiceType,
    weekday: number,
    weekdayModalVisible: boolean,
}

class InvoiceTemplateScreen extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        const id = this.props.navigation.getParam('id');
        const invoiceTemplate = this.props.invoiceTemplates.find(invoiceTemplate => invoiceTemplate.id === id);
        const firstSellerId = this.props.sellers[0] ? this.props.sellers[0].id : null;
        this.state = {
            clientId: invoiceTemplate ? invoiceTemplate.clientId : null,
            clientsModalVisible: false,
            id,
            name: invoiceTemplate ? invoiceTemplate.name : '',
            productRecords: invoiceTemplate ? invoiceTemplate.productRecords : [],
            productsModalVisible: false,
            sellerId: invoiceTemplate ? invoiceTemplate.sellerId : firstSellerId,
            sellersModalVisible: false,
            type: invoiceTemplate ? invoiceTemplate.type : INVOICE_TYPE_SALE,
            weekday: invoiceTemplate ? invoiceTemplate.weekday : WEEKDAY_OPTIONS[0].id,
            weekdayModalVisible: false,
        };
    }

    onClientPickerPress = clientId => {
        this.setState({ clientId });
        this.hideClientsModal();
    };

    showClientsModal = () => this.setState({ clientsModalVisible: true });

    hideClientsModal = () => this.setState({ clientsModalVisible: false });

    onSellerPickerPress = sellerId => {
        this.setState({ sellerId });
        this.hideSellersModal();
    };

    showSellersModal = () => this.setState({ sellersModalVisible: true });

    hideSellersModal = () => this.setState({ sellersModalVisible: false });

    onWeekdayPickerPress = weekday => {
        this.setState({ weekday });
        this.hideWeekdayModal();
    };

    showWeekdayModal = () => this.setState({ weekdayModalVisible: true });

    hideWeekdayModal = () => this.setState({ weekdayModalVisible: false });

    onProductPickerPress = productId => {
        const productRecords = [...this.state.productRecords, {
            productId,
            quantity: '0',
        }];
        this.setState({ productRecords });
        this.hideProductsModal();
    };

    showProductsModal = () => this.setState({ productsModalVisible: true });

    hideProductsModal = () => this.setState({ productsModalVisible: false });

    onProductQuantityAddPress = (quantityToAdd, index) => {
        const quantity = Number(this.state.productRecords[index].quantity) + quantityToAdd;
        const newQuantity = quantity < 0 ? 0 : quantity;
        this.onProductRecordFieldChange('quantity', String(newQuantity), index);
    };

    onProductRecordFieldChange = (field, value, index) => {
        this.setState({ productRecords: [
                ...this.state.productRecords.slice(0, index),
                {
                    ...this.state.productRecords[index],
                    [field]: value,
                },
                ...this.state.productRecords.slice(index + 1),
            ],
        });
    };

    onProductRecordRemovePress = index => {
        this.setState({
            productRecords: [
                ...this.state.productRecords.slice(0, index),
                ...this.state.productRecords.slice(index + 1),
            ]
        });
    };

    onInvoiceTypePress = type => {
        if (type === INVOICE_TYPE_PURCHASE) {
            this.setState({ clientId: null });
        }
        this.setState({ type });
    };

    clearForm = () =>
        this.setState({
            clientId: null,
            clientsModalVisible: false,
            name: '',
            productRecords: [],
            productsModalVisible: false,
            sellerId: null,
            sellersModalVisible: false,
            type: INVOICE_TYPE_SALE,
            weekday: WEEKDAY_OPTIONS[0].id,
            weekdayModalVisible: false,
        });

    get isFormValid () {
        const isClientFieldValid = this.state.type === INVOICE_TYPE_PURCHASE || this.state.clientId !== null;
        return this.state.productRecords.length > 0 && isClientFieldValid && this.state.sellerId !== null;
    }

    onSubmitButtonPress = () => {
        if (!this.isFormValid) return;

        if (this.state.id) {
            this.onSavePress();
            return;
        }
        this.onAddPress();
    };

    get availableProducts () {
        const chosenProductsIds = this.state.productRecords.map(productRecord => productRecord.productId);
        return this.props.products.filter(product => !chosenProductsIds.includes(product.id));
    }

    get invoiceTemplateConfig (): InvoiceTemplateConfig {
        return {
            clientId: this.state.clientId,
            name: this.state.name !== '' ? this.state.name : this.generatedTemplateName,
            productRecords: this.state.productRecords,
            sellerId: this.state.sellerId,
            type: this.state.type,
            weekday: this.state.weekday,
        }
    }

    onAddPress = () => {
        this.props.addInvoiceTemplate(createInvoiceTemplate(this.invoiceTemplateConfig));
        this.clearForm();
        this.props.navigation.goBack();
    };

    onSavePress = () => {
        this.props.updateInvoiceTemplate(this.state.id, this.invoiceTemplateConfig);
        this.props.navigation.goBack();
    };

    get generatedTemplateName () {
        const {
            clientsById,
            sellersById,
        } = this.props;
        let placeholder = '';
        if (this.state.type === INVOICE_TYPE_PURCHASE) {
            if (this.state.sellerId !== null) {
                placeholder += sellersById[this.state.sellerId].name + ' - ';
            }
        } else {
            if (this.state.sellerId !== null && this.state.clientId !== null) {
                placeholder += sellersById[this.state.sellerId].name + ' - ' + clientsById[this.state.clientId].name + ' - ';
            }
        }
        placeholder += this.state.type === INVOICE_TYPE_PURCHASE ? 'Приходная' : 'Расходная';
        placeholder += ' - ';
        placeholder += getWeekdayName(this.state.weekday);
        return placeholder;
    }

    render () {
        const {
            clients,
            clientsById,
            productsById,
            sellers,
            sellersById,
        } = this.props;
        const {
            clientId,
            productRecords,
            sellerId,
        } = this.state;
        const client = clientsById[clientId];
        const seller = sellersById[sellerId];

        return (
            <View style={styles.container}>
                <KeyboardAvoidingView style={{ flex: 1 }} behavior='height'>
                    <ScrollView style={{ flex: 1 }}>
                        <Input
                            label='Название'
                            placeholder={this.generatedTemplateName}
                            value={this.state.name}
                            onChangeText={name => this.setState({ name })}
                        />
                        <ButtonGroup
                            onPress={this.onInvoiceTypePress}
                            selectedIndex={this.state.type}
                            buttons={['Приходная', 'Расходная']}
                            containerStyle={{ marginBottom: 10 }}
                        />
                        { this.state.type === INVOICE_TYPE_SALE && (
                            <EntityButton
                                containerStyle={{ marginBottom: 20 }}
                                onPress={this.showClientsModal}
                                title='Клиент'
                                subtitle={client ? client.name : ''}
                            />
                        )}
                        <EntityButton
                            containerStyle={{ marginBottom: 20 }}
                            onPress={this.showSellersModal}
                            title='Продавец'
                            subtitle={seller ? seller.name : ''}
                        />
                        <EntityButton
                            containerStyle={{ marginBottom: 20 }}
                            onPress={this.showWeekdayModal}
                            title='День недели'
                            subtitle={getWeekdayName(this.state.weekday)}
                        />
                        {productRecords.map((productRecord, index) => (
                            <ProductItem
                                containerStyle={{ marginBottom: 20 }}
                                key={productRecord.productId}
                                onAddPress={value => this.onProductQuantityAddPress(value, index)}
                                onQuantityChange={value => this.onProductRecordFieldChange('quantity', value, index)}
                                onRemovePress={() => this.onProductRecordRemovePress(index)}
                                showPrice={false}
                                quantity={productRecord.quantity}
                                title={productsById[productRecord.productId].name}
                            />
                        ))}
                    </ScrollView>
                    <Button
                        containerStyle={{ marginBottom: 10 }}
                        onPress={this.showProductsModal}
                        title='Добавить товар'
                    />
                    <Button
                        onPress={this.onSubmitButtonPress}
                        title='Сохранить'
                        buttonStyle={{ backgroundColor: '#9add83' }}
                    />
                </KeyboardAvoidingView>
                <Modal visible={this.state.clientsModalVisible}>
                    <ItemPicker
                        items={clients}
                        onPress={this.onClientPickerPress}
                        close={this.hideClientsModal}
                    />
                </Modal>
                <Modal visible={this.state.sellersModalVisible}>
                    <ItemPicker
                        items={sellers}
                        onPress={this.onSellerPickerPress}
                        close={this.hideSellersModal}
                    />
                </Modal>
                <Modal visible={this.state.productsModalVisible}>
                    <ItemPicker
                        items={this.availableProducts}
                        onPress={this.onProductPickerPress}
                        close={this.hideProductsModal}
                    />
                </Modal>
                <Modal visible={this.state.weekdayModalVisible}>
                    <ItemPicker
                        items={WEEKDAY_OPTIONS}
                        onPress={this.onWeekdayPickerPress}
                        close={this.hideWeekdayModal}
                    />
                </Modal>
            </View>
        );
    }
}

InvoiceTemplateScreen.navigationOptions = {
    title: 'Шаблон накладной',
};

export default InvoiceTemplateScreen;