// @flow

import uuid from 'uuid';
import type {
    Invoice,
    InvoiceConfig,
} from 'flow/types';

const createInvoice = (config: InvoiceConfig = {}): Invoice => ({
    clientId: null,
    createDate: new Date().toISOString(),
    id: uuid(),
    productRecords: [],
    sellerId: null,
    ...config,
});

export default createInvoice;