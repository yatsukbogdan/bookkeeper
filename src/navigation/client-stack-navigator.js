import { createStackNavigator } from 'react-navigation';

import ClientListScreen from '../screens/client-list';
import ClientScreen from '../screens/client';

const ClientStackNavigator = createStackNavigator({
    ClientList: ClientListScreen,
    Client: ClientScreen,
});

ClientStackNavigator.navigationOptions = {
    tabBarLabel: 'Клиенты'
};

ClientStackNavigator.path = '';

export default ClientStackNavigator;
