import type {
    Client,
    ClientConfig,
} from 'flow/types';

export const ADD_CLIENT = 'ADD_CLIENT';
export const UPDATE_CLIENT = 'UPDATE_CLIENT';
export const REMOVE_CLIENT = 'REMOVE_CLIENT';
export const SET_CLIENTS = 'SET_CLIENTS';

export const addClient = (client: Client) => ({
    type: ADD_CLIENT,
    payload: {
        client
    }
});

export const updateClient = (id: string, data: ClientConfig) => ({
    type: UPDATE_CLIENT,
    payload: {
        id,
        data,
    }
});

export const removeClient = (id: string) => ({
    type: REMOVE_CLIENT,
    payload: {
        id
    }
});

export const setClients = (clients: Array<Client>) => ({
    type: SET_CLIENTS,
    payload: {
        clients
    }
});