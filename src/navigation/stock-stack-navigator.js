import { createStackNavigator } from 'react-navigation';

import StockScreen from '../screens/stock';

const StockStackNavigator = createStackNavigator({
    Stock: StockScreen,
});

StockStackNavigator.navigationOptions = {
    tabBarLabel: 'Остатки'
};

StockStackNavigator.path = '';

export default StockStackNavigator;
