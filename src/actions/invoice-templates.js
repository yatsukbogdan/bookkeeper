import type {
    InvoiceTemplate,
    InvoiceTemplateConfig,
} from 'flow/types';

export const ADD_INVOICE_TEMPLATE = 'ADD_INVOICE_TEMPLATE';
export const UPDATE_INVOICE_TEMPLATE = 'UPDATE_INVOICE_TEMPLATE';
export const REMOVE_INVOICE_TEMPLATE = 'REMOVE_INVOICE_TEMPLATE';
export const SET_INVOICE_TEMPLATES = 'SET_INVOICES_TEMPLATE';

export const addInvoiceTemplate = (invoiceTemplate: InvoiceTemplate) => ({
    type: ADD_INVOICE_TEMPLATE,
    payload: {
        invoiceTemplate
    }
});

export const updateInvoiceTemplate = (id: string, data: InvoiceTemplateConfig) => ({
    type: UPDATE_INVOICE_TEMPLATE,
    payload: {
        id,
        data,
    }
});


export const removeInvoiceTemplate = (id: string) => ({
    type: REMOVE_INVOICE_TEMPLATE,
    payload: {
        id
    }
});

export const setInvoiceTemplates = (invoiceTemplates: Array<InvoiceTemplate>) => ({
    type: SET_INVOICE_TEMPLATES,
    payload: {
        invoiceTemplates
    }
});