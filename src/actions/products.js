import type {
    Product,
    ProductConfig,
} from 'flow/types';

export const ADD_PRODUCT = 'ADD_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';
export const SET_PRODUCTS = 'SET_PRODUCTS';

export const addProduct = (product: Product) => ({
    type: ADD_PRODUCT,
    payload: {
        product
    }
});

export const updateProduct = (id: string, data: ProductConfig) => ({
    type: UPDATE_PRODUCT,
    payload: {
        id,
        data,
    }
});


export const removeProduct = (id: string) => ({
    type: REMOVE_PRODUCT,
    payload: {
        id
    }
});

export const setProducts = (products: Array<Product>) => ({
    type: SET_PRODUCTS,
    payload: {
        products
    }
});