// @flow

import React from 'react';
import { ScrollView, View } from 'react-native';
import {ListItem, Button, Input} from 'react-native-elements';
import styles from './styles'
import type { Product } from 'flow/types';

type Props = {
    products: Array<Product>
}

const ProductListScreen = (props: Props) => (
    <View style={{flex: 1}}>
        <ScrollView style={styles.container}>
            {props.products.map(product => (
                <ListItem
                    key={product.id}
                    title={product.name}
                    onPress={() => props.navigation.push('Product', { id: product.id })}
                />
            ))}
        </ScrollView>
        <Button
            containerStyle={{
                marginBottom: 10,
                marginHorizontal: 10,
            }}
            onPress={() => props.navigation.push('Product')}
            title='Добавить новый товар'
        />
    </View>
);

ProductListScreen.navigationOptions = {
    title: 'Список товаров',
};

export default ProductListScreen;