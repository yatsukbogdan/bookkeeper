// @flow

import { connect } from 'react-redux';
import View from './view';
import {
    addSeller,
    removeSeller,
    updateSeller,
} from 'actions/sellers';

const mapStateToProps = (state: State) => ({
    sellers: state.sellers,
});

const mapDispatchToProps = {
    addSeller,
    removeSeller,
    updateSeller,
};

export default connect(mapStateToProps, mapDispatchToProps)(View);