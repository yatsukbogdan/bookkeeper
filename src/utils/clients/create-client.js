// @flow

import uuid from 'uuid';
import type {
    Client,
    ClientConfig,
} from 'flow/types';

const createClient = (config: ClientConfig = {}): Client => ({
    id: uuid(),
    name: '',
    ...config,
});

export default createClient;