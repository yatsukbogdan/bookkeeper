// @flow

import uuid from 'uuid';
import type {
    Seller,
    SellerConfig,
} from 'flow/types';

const createSeller = (config: SellerConfig = {}): Seller => ({
    id: uuid(),
    name: '',
    phoneNumber: '',
    ...config,
});

export default createSeller;
