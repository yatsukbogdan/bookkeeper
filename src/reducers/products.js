import {
    ADD_PRODUCT,
    REMOVE_PRODUCT,
    UPDATE_PRODUCT,
    SET_PRODUCTS,
} from 'actions/products';
import type { ProductsState } from 'flow/types';

const initialState = [];

const products = (state: ProductsState = initialState, action) => {
    switch (action.type) {
        case ADD_PRODUCT: return [...state, action.payload.product];
        case UPDATE_PRODUCT: {
            const productIndex = state.findIndex(product => product.id === action.payload.id);
            if (productIndex === -1) return state;
            return [
                ...state.slice(0, productIndex),
                {
                    ...state[productIndex],
                    ...action.payload.data,
                },
                ...state.slice(productIndex + 1),
            ];
        }
        case REMOVE_PRODUCT: {
            const productIndex = state.findIndex(product => product.id === action.payload.id);
            if (productIndex === -1) return state;
            return [
                ...state.slice(0, productIndex),
                ...state.slice(productIndex + 1),
            ];
        }
        case SET_PRODUCTS: return action.payload.products;
        default:
            return state;
    }
};

export default products;