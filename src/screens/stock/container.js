// @flow

import { connect } from 'react-redux';
import View from './view';
import generateObjectFromArray from 'utils/generate-object-from-array';

const mapStateToProps = (state: State) => ({
    invoices: state.invoices,
    productsById: generateObjectFromArray(state.products),
});

export default connect(mapStateToProps)(View);