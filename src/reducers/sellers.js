import {
    ADD_SELLER,
    REMOVE_SELLER, SET_SELLERS,
    UPDATE_SELLER,
} from 'actions/sellers';
import type { SellersState } from 'flow/types';

const initialState = [];

const sellers = (state: SellersState = initialState, action) => {
    switch (action.type) {
        case ADD_SELLER: return [...state, action.payload.seller];
        case UPDATE_SELLER: {
            const sellerIndex = state.findIndex(seller => seller.id === action.payload.id);
            if (sellerIndex === -1) return state;
            return [
                ...state.slice(0, sellerIndex),
                {
                    ...state[sellerIndex],
                    ...action.payload.data,
                },
                ...state.slice(sellerIndex + 1),
            ];
        }
        case REMOVE_SELLER: {
            const sellerIndex = state.findIndex(seller => seller.id === action.payload.id);
            if (sellerIndex === -1) return state;
            return [
                ...state.slice(0, sellerIndex),
                ...state.slice(sellerIndex + 1),
            ];
        }
        case SET_SELLERS: return action.payload.sellers;
        default:
            return state;
    }
};

export default sellers;