// @flow

import uuid from 'uuid';
import type {
    InvoiceTemplate,
    InvoiceTemplateConfig,
} from 'flow/types';
import { WEEKDAY_MONDAY } from 'constants/weekdays';
import { INVOICE_TYPE_PURCHASE } from 'constants/invoice';

const createInvoiceTemplate = (config: InvoiceTemplateConfig = {}): InvoiceTemplate => ({
    clientId: null,
    id: uuid(),
    name: '',
    productRecords: [],
    sellerId: null,
    type: INVOICE_TYPE_PURCHASE,
    weekday: WEEKDAY_MONDAY,
    ...config,
});

export default createInvoiceTemplate;