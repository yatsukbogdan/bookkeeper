// @flow

import React from 'react';
import {
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import type { StyleProp } from 'react-native'
import styles from './styles'

type Props = {
    containerStyle: StyleProp,
    onPress: Function,
    title: string,
    type: 'add' | 'subtract',
}

const RoundButton = (props: Props) => (
    <View style={props.containerStyle}>
        <TouchableOpacity onPress={props.onPress}>
            <View style={[
                styles.container, {
                    backgroundColor: props.type === 'add' ? '#90ee92' : '#ee6f6b',
                }
            ]}>
                <Text style={styles.title}>{props.title}</Text>
            </View>
        </TouchableOpacity>
    </View>
);

RoundButton.defaultProps = {
    containerStyle: {},
};

export default RoundButton;