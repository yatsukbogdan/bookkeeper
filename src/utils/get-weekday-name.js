const getWeekdayName = weekday => {
    switch (weekday) {
        case 0: return 'Воскресение';
        case 1: return 'Понедельник';
        case 2: return 'Вторник';
        case 3: return 'Среда';
        case 4: return 'Четверг';
        case 5: return 'Пятница';
        case 6:
        default: return 'Суббота';
    }
};

export  default getWeekdayName;