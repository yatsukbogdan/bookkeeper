import type {
    Invoice,
    InvoiceConfig,
} from 'flow/types';

export const ADD_INVOICE = 'ADD_INVOICE';
export const UPDATE_INVOICE = 'UPDATE_INVOICE';
export const REMOVE_INVOICE = 'REMOVE_INVOICE';
export const SET_INVOICES = 'SET_INVOICES';

export const addInvoice = (invoice: Invoice) => ({
    type: ADD_INVOICE,
    payload: {
        invoice
    }
});

export const updateInvoice = (id: string, data: InvoiceConfig) => ({
    type: UPDATE_INVOICE,
    payload: {
        id,
        data,
    }
});


export const removeInvoice = (id: string) => ({
    type: REMOVE_INVOICE,
    payload: {
        id
    }
});

export const setInvoices = (invoices: Array<Invoice>) => ({
    type: SET_INVOICES,
    payload: {
        invoices
    }
});