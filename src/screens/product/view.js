// @flow

import React from 'react';
import { ScrollView } from 'react-native';
import {
    Input,
    Button,
} from 'react-native-elements';
import styles from './styles'
import createProduct from 'utils/products/create-product';
import { TextInputMask } from 'react-native-masked-text';
import type {
    Product,
    ProductConfig,
} from 'flow/types';

type Props = {
    addProduct: (product: Product) => void,
    products: Array<Product>,
    removeProduct: (id: string) => void,
    updateProduct: (id: string, data: ProductConfig) => void,
}

type State = {
    id: string,
    nameError: string,
    nameValue: string,
    purchasePriceError: string,
    purchasePriceValue: string,
    salePriceError: string,
    salePriceValue: string,
}

class ProductScreen extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        const id = this.props.navigation.getParam('id');
        const product = this.props.products.find(product => product.id === id);
        this.state = {
            id,
            nameError: '',
            nameValue: product ? product.name : '',
            purchasePriceError: '',
            purchasePriceValue: product ? String(product.purchasePrice) : '',
            salePriceError: '',
            salePriceValue: product ? String(product.salePrice) : '',
        };
    }

    validateName = () => {
        const value = this.state.nameValue;
        if (value.length < 2) {
            this.setState({ nameError: 'Наименование должно быть больше 2 символов' });
            return;
        }
        const otherProducts = this.state.id ?
            this.props.products.filter(product => product.id !== this.state.id) :
            this.props.products;

        if (otherProducts.find(product => product.name === value)) {
            this.setState({ nameError: 'Товар с таким именем существует' });
            return;
        }
        this.setState({ nameError: '' });
    };

    validatePurchasePrice = () => {

    };

    validateSalePrice = () => {

    };

    validateField = field => {
        switch (field) {
            case 'name': return this.validateName();
            case 'purchasePrice': return this.validatePurchasePrice();
            case 'salePrice': return this.validateSalePrice();
            default: return;
        }
    };

    onChange = (field, value) => {
        if (field === 'purchasePrice' || field === 'salePrice') {
            const valueWithDot = value.replace(',', '.');
            const valuesWithDotSplited = valueWithDot.split('.');
            if (valuesWithDotSplited.length > 2 || (valuesWithDotSplited[1] && valuesWithDotSplited[1].length > 2)) return;
            this.setState({
                [`${field}Value`]: valueWithDot
            }, () => this.validateField(field));
            return;
        }
        this.setState({
            [`${field}Value`]: value
        }, () => this.validateField(field));
    };

    clearForm = () =>
        this.setState({
            nameError: '',
            nameValue: '',
            purchasePriceError: '',
            purchasePriceValue: '',
            salePriceError: '',
            salePriceValue: '',
        });

    get isFormValid () {
        return !this.state.nameError && !this.state.purchasePriceError && !this.state.salePriceError;
    }

    onSubmitButtonClick = () => {
        if (!this.isFormValid) return;

        if (this.state.id) {
            this.onSaveClick();
            return;
        }
        this.onAddClick();
    };

    get productConfig (): ProductConfig {
        return {
            name: this.state.nameValue,
            purchasePrice: this.state.purchasePriceValue,
            salePrice: this.state.salePriceValue,
        }
    }

    onAddClick = () => {
        this.props.addProduct(createProduct(this.productConfig));
        this.clearForm();
        this.props.navigation.goBack();
    };

    onSaveClick = () => {
        this.props.updateProduct(this.state.id, this.productConfig);
        this.props.navigation.goBack();
    };

    render () {
        return (
            <ScrollView style={styles.container}>
                <Input
                    containerStyle={{ marginBottom: 20 }}
                    errorMessage={this.state.nameError}
                    label='Наименование'
                    onChangeText={value => this.onChange('name', value)}
                    onSubmitEditing={() => this.purchasePrice.focus()}
                    returnKeyType='next'
                    value={this.state.nameValue}
                />
                <Input
                    containerStyle={{ marginBottom: 20 }}
                    errorMessage={this.state.purchasePriceError}
                    keyboardType='numeric'
                    label='Цена закупки'
                    onChangeText={value => this.onChange('purchasePrice', value)}
                    onSubmitEditing={() => this.salePrice.focus()}
                    ref={input => this.purchasePrice = input}
                    returnKeyType='next'
                    value={this.state.purchasePriceValue}
                />
                <Input
                    errorMessage={this.state.salePriceError}
                    keyboardType='numeric'
                    label='Цена продажи'
                    onChangeText={value => this.onChange('salePrice', value)}
                    ref={input => this.salePrice = input}
                    returnKeyType='done'
                    value={this.state.salePriceValue}
                    containerStyle={{ marginBottom: 20 }}
                />
                <Button
                    containerStyle={{ marginHorizontal: 10 }}
                    onPress={this.onSubmitButtonClick}
                    title='Сохранить'
                    buttonStyle={{ backgroundColor: '#9add83' }}
                />
            </ScrollView>
        );
    }
}

ProductScreen.navigationOptions = {
    title: 'Товары',
};

export default ProductScreen;