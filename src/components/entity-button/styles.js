import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        height: 50,
        paddingHorizontal: 20,
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#eee',
    },
    title: {
        fontSize: 20,
    },
    subtitle: {
        fontSize: 20,
        color: '#555',
    }
});