// @flow

import React from 'react';
import { ScrollView } from 'react-native';
import {
    Input,
    Button,
} from 'react-native-elements';
import styles from './styles'
import createClient from 'utils/clients/create-client';
import type {
    Client,
    ClientConfig,
} from 'flow/types';

type Props = {
    addClient: (product: Client) => void,
    clients: Array<Client>,
    removeClient: (id: string) => void,
    updateClient: (id: string, data: ClientConfig) => void,
}

type State = {
    id: string,
    nameError: string,
    nameValue: string,
}

class ClientScreen extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        const id = this.props.navigation.getParam('id');
        const client = this.props.clients.find(client => client.id === id);
        this.state = {
            id,
            nameError: '',
            nameValue: client ? client.name : '',
        };
    }

    validateName = () => {
        const value = this.state.nameValue;
        if (value.length < 2) {
            this.setState({ nameError: 'Имя должно быть длинее чем два символа' });
            return;
        }
        const otherClients = this.state.id ?
            this.props.clients.filter(client => client.id !== this.state.id) :
            this.props.clients;

        if (otherClients.find(client => client.name === value)) {
            this.setState({ nameError: 'Клиент с таким именем уже существует' });
            return;
        }
        this.setState({ nameError: '' });
    };

    onNameChange = value => this.setState({ nameValue: value }, this.validateName);

    clearForm = () =>
        this.setState({
            nameError: '',
            nameValue: '',
        });

    get isFormValid () {
        return !this.state.nameError;
    }

    onSubmitButtonClick = () => {
        if (!this.isFormValid) return;

        if (this.state.id) {
            this.onSaveClick();
            return;
        }
        this.onAddClick();
    };

    get clientConfig (): ClientConfig {
        return {
            name: this.state.nameValue,
        }
    }

    onAddClick = () => {
        this.props.addClient(createClient(this.clientConfig));
        this.clearForm();
        this.props.navigation.goBack();
    };

    onSaveClick = () => {
        this.props.updateClient(this.state.id, this.clientConfig);
        this.props.navigation.goBack();
    };

    render () {
        return (
            <ScrollView style={styles.container}>
                <Input
                    containerStyle={{ marginBottom: 20 }}
                    errorMessage={this.state.nameError}
                    label='Имя'
                    onChangeText={this.onNameChange}
                    returnKeyType='done'
                    value={this.state.nameValue}
                />
                <Button
                    containerStyle={{ marginHorizontal: 10 }}
                    buttonStyle={{ backgroundColor: '#9add83' }}
                    onPress={this.onSubmitButtonClick}
                    title='Сохранить'
                />
            </ScrollView>
        );
    }
}

ClientScreen.navigationOptions = {
    title: 'Клиент',
};

export default ClientScreen;